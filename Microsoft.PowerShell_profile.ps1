$PROFILE_PATH = Split-Path -Path $PROFILE

Import-Module WieldingLs

Set-PSReadlineOption -ExtraPromptLineCount 1
Set-PSReadlineOption -EditMode Emacs 
Set-PSReadlineOption -HistoryNoDuplicates
Set-PSReadlineOption -BellStyle None

oh-my-posh init pwsh --config $(Join-Path -Path $PROFILE_PATH -ChildPath "theme.omp.json") | Invoke-Expression

$GdcTheme.DefaultDisplayFormat = "Short"

foreach ($key in @($GdcTheme.FileAttributesColors.Keys)) {
  switch ($key) {
    "Directory, ReparsePoint" { $GdcTheme.FileAttributesColors[$key] = "!{:F14:}" }
    "Directory" { $GdcTheme.FileAttributesColors[$key] = "{:F12:}" }
    "Hidden, System, Directory" { $GdcTheme.FileAttributesColors[$key] = "" }
    "Hidden" { $GdcTheme.FileAttributesColors[$key] = "" }
  }
}

$GdcTheme.SourceCodeColor = ""
$GdcTheme.DataFileColor = ""
$GdcTheme.LogFileColor = ""
$GdcTheme.CompressedFileColor = "{:F1:}"
$GdcTheme.ImageFileColor = "{:F13:}"
$GdcTheme.ExecutableFileColor = "{:F10:}"
$GdcTheme.DocumentFileColor = ""
$GdcTheme.HiddenFileColor = "{:F8:}"
$GdcTheme.HiddenFolderColor = "{:F4:}"
$GdcTheme.NakedFileColor = ""
$GdcTheme.DefaultFileColor = ""
$GdcTheme.LastWriteTime = ""

Update-GDCColors

Remove-Item alias:dir -ErrorAction SilentlyContinue
Remove-Item alias:ls -ErrorAction SilentlyContinue

Set-Alias -Name ls -Value Get-DirectoryContents
Set-Alias -Name touch -Value New-Item
function dir { Get-DirectoryContents -l @args }
