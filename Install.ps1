function InstallOrUpdate([string] $module) {
  if ((Get-InstalledModule $module 2>&1) -like "*No match was found*") {
    Write-Host "Installing $module"
    PowerShellGet\Install-Module $module -Scope CurrentUser -Force
  }
  else {
    Write-Host "Updating $module"
    PowerShellGet\Update-Module $module
  }
}

InstallOrUpdate WieldingLs
winget install JanDeDobbeleer.OhMyPosh -s winget